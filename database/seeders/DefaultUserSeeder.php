<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        User::firstOrCreate([
            'email' => 'test@mail.ru',
        ], [
            'name'      => 'Тестовый пользователь',
            'password'  => Hash::make('test'),
            'email_verified_at' => Carbon::now(),
        ]);

    }
}
