<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Settings\Setting;
use Illuminate\Database\Seeder;

class DefaultSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Setting::firstOrCreate([
            'weather_key' => '6397bf0d4ac8a49ef3542719b101ba14',
            'parse_at' => '00:00'
        ]);
    }
}
