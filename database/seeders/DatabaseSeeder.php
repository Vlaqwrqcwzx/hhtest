<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(DefaultSettingsSeeder::class);
        $this->call(DefaultUserSeeder::class);
        $this->call(DefaultCitiesSeeder::class);
    }
}
