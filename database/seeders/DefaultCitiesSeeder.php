<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Cities\City;
use Illuminate\Database\Seeder;

class DefaultCitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        City::firstOrCreate([
            'name' => 'Атяшево'
        ]);

        City::firstOrCreate([
            'name' => 'Саранск'
        ]);

        City::firstOrCreate([
            'name' => 'Москва'
        ]);
    }
}
