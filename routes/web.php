<?php

use App\Http\Controllers\Cities\CityController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Settings\SettingController;
use App\Http\Controllers\Users\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
   return redirect()->route('login');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::resource('cities',CityController::class);
    Route::resource('users',UserController::class);
    Route::any('settings',SettingController::class);
});


