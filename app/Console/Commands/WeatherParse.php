<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Cities\City;
use App\Services\Weather\Weather;
use Illuminate\Console\Command;

class WeatherParse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command update city weather information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $weather = new Weather();
        $cities = City::all();
        foreach ($cities as $city) {
            $city->update(['weather' => $weather->get($city->name)->getContents()]);
        }
        return 0;
    }
}
