<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'      => 'required|string|max:255',
            'email'     => 'required|string|email|max:255',Rule::unique('users')->ignore($this->id),
            'password'  => 'required|string|min:8|confirmed'
        ];
    }

    /**
     * @return string[] array
     */
    public function messages()
    {
        return [
            'name.required'                   => __('users.validation.name.required'),
            'name.string'                     => __('users.validation.name.string'),
            'name.max'                        => __('users.validation.name.max'),
            'email.required'                  => __('users.validation.email.required'),
            'email.string'                    => __('users.validation.email.string'),
            'email.email'                     => __('users.validation.email.email'),
            'email.max'                       => __('users.validation.email.max'),
            'email.unique'                    => __('users.validation.email.unique'),
            'password.required'               => __('users.validation.password.required'),
            'password.string'                 => __('users.validation.password.string'),
            'password.min'                    => __('users.validation.password.min'),
            'password.confirmed'              => __('users.validation.password.confirmed'),
        ];
    }
}
