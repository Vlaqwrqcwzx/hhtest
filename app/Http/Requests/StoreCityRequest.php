<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'  => 'required|string|max:255',
            'code'  => 'nullable|string|max:255',
        ];
    }

    /**
     * @return string[] array
     */
    public function messages()
    {
        return [
            'name.required'                   => __('cities.validation.name.required'),
            'name.string'                     => __('cities.validation.name.string'),
            'name.max'                        => __('cities.validation.name.max'),
            'code.string'                     => __('cities.validation.code.string'),
            'code.max'                        => __('cities.validation.code.max'),
        ];
    }
}
