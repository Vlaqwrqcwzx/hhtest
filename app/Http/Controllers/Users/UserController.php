<?php

declare(strict_types=1);

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use App\Services\Users\UserService as Service;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function __construct()
    {
        $this->service = app(Service::class);
    }

    public function create(): JsonResponse
    {
        return $this->createElementModal();
    }

    public function store(StoreUserRequest $request): JsonResponse
    {
        return $this->storeElement($request->validated());
    }

    public function edit(User $user): JsonResponse
    {
        return $this->editElementModal($user);
    }

    public function show(User $user): JsonResponse
    {
        return $this->showElementModal($user);
    }

    public function update(StoreUserRequest $request, User $user): JsonResponse
    {
        return $this->updateElement($request->validated(), $user);
    }

    public function destroy(User $user): JsonResponse
    {
        return $this->destroyElement($user);
    }
}
