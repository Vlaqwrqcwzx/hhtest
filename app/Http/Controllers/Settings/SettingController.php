<?php

declare(strict_types=1);

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SettingController extends Controller
{
    public function __invoke(Request $request): View
    {
        if ($request->isMethod('PATCH')) {
            $settings = Setting::first();
            $settings->update($request->all());
        }
        return view('settings.index', ['settings' => Setting::first()]);
    }
}
