<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $service;

    public function index()
    {
        if (request()->ajax()) {
            return $this->service->getDatatableElements();
        }

        $with = $this->service->outputData();
        $with['title'] = __($this->service->translationKeys . 'index.title');
        $with['columns'] = $this->service->getTableColumns();
        $with['jsonColumns'] = $this->service->getJsonTableColumns();

        return view($this->service->templatePath . 'index')->with($with);
    }

    public function createElementModal(Model $model = null): JsonResponse
    {
        $title = __($this->service->translationKeys . 'create.title');
        $with = $this->service->getElementData($model);
        $with['method'] = 'create';

        return response()->json([
            'action' => 'success',
            'html' => view($this->service->templatePath . 'form')->with($with)->render(),
            'title' => $title,
            'button' => __($this->service->translationKeys . 'create.button'),
        ]);
    }

    public function storeElement($request): JsonResponse
    {
        $store = $this->service->storeRecord($request);

        if ($store) {
            return response()->json([
                'action' => 'reload_table',
                'success' => 'Успешно сохранено'
            ]);
        }

        return response()->json([
            'error' => 'При сохранении возникла ошибка'
        ]);
    }

    public function showElementModal(Model $model): JsonResponse
    {
        $title = __($this->service->translationKeys . 'edit.title');
        $with = $this->service->getElementData($model);
        $with['method'] = 'show';

        return response()->json([
            'action' => 'success',
            'html' => view($this->service->templatePath . 'form')->with($with)->render(),
            'title' => $title,
            'button' => __($this->service->translationKeys . 'edit.button'),
        ]);
    }

    public function editElementModal(Model $model): JsonResponse
    {
        $title = __($this->service->translationKeys . 'edit.title');
        $with = $this->service->getElementData($model);
        $with['method'] = 'edit';

        return response()->json([
            'action' => 'success',
            'html' => view($this->service->templatePath . 'form')->with($with)->render(),
            'title' => $title,
            'button' => __($this->service->translationKeys . 'edit.button'),
        ]);
    }

    public function updateElement($request, $element): JsonResponse
    {
        $update = $this->service->updateRecord($request, $element);

        if ($update) {
            return response()->json([
                'action' => 'reload_table',
                'success' => 'Успешно обновлено'
            ]);
        }

        return response()->json([
            'error' => 'При обновлении возникла ошибка'
        ]);
    }

    public function destroyElement($element): JsonResponse
    {
        $element->delete();

        return response()->json([
            'action' => 'reload_table'
        ]);
    }
}
