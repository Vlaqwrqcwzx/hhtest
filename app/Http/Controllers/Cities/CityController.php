<?php

declare(strict_types=1);

namespace App\Http\Controllers\Cities;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCityRequest;
use App\Models\Cities\City;
use App\Services\Cities\CityService as Service;
use Illuminate\Http\JsonResponse;

class CityController extends Controller
{
    public function __construct()
    {
        $this->service = app(Service::class);
    }

    public function create(): JsonResponse
    {
        return $this->createElementModal();
    }

    public function store(StoreCityRequest $request): JsonResponse
    {
        return $this->storeElement($request->validated());
    }

    public function edit(City $city): JsonResponse
    {
        return $this->editElementModal($city);
    }

    public function show(City $city): JsonResponse
    {
        return $this->showElementModal($city);
    }

    public function update(StoreCityRequest $request, City $city): JsonResponse
    {
        return $this->updateElement($request->validated(), $city);
    }

    public function destroy(City $city): JsonResponse
    {
        return $this->destroyElement($city);
    }
}
