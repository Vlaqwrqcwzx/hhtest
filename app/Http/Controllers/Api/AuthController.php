<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\TokenResource;
use Illuminate\Support\Facades\Auth;
use App\Services\Users\UserService;

class AuthController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function login(LoginRequest $request): TokenResource
    {
        $email = $request->get('email');
        $password = $request->get('password');

        if (!Auth::attempt(['email' => $email, 'password' => $password])) {
            abort(401, 'Invalid data');
        }

        $user = Auth::user();
        $user = $this->userService->createToken($user);

        return new TokenResource($user);
    }
}
