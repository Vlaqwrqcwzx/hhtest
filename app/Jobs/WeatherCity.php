<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\Cities\City;
use App\Services\Weather\Weather;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WeatherCity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var City
     */
    private $city;

    /**
     * Create a new job instance.
     *
     * @param City $city
     */
    public function __construct(City $city)
    {
        $this->city = $city;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $weather = new Weather();
        $this->city->update([
            'weather' => $weather->get($this->city->name)->getContents()
        ]);
    }
}
