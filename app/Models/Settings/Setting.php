<?php

declare(strict_types=1);

namespace App\Models\Settings;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = [
        'weather_key',
        'parse_at'
    ];

    public function getParseAtAttribute($value): string
    {
        return $value ? Carbon::parse($value)->format('H:i') : '';
    }
}
