<?php

declare(strict_types=1);

namespace App\Services\Cities;

use App\Jobs\WeatherCity;
use App\Models\Cities\City;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\DataTables;

class CityService extends BaseService
{
    private $model;
    public $nameController = 'cities';
    public $templatePath = 'cities.';
    public $templateForm = 'create';
    public $translationKeys = 'cities.';

    private CONST T0 = 273.15;

    public function __construct()
    {
        $this->model = City::query();
    }

    public function getElementData(City $city = null): array
    {
        return compact('city');
    }

    public function getTableColumns(): array
    {
        return [
            [
                'data'          => 'id',
                'column_name'   => __($this->translationKeys . 'datatable.id'),
                'name'          => 'id',
                'width'         => '10%'
            ],
            [
                'data'          => 'name',
                'column_name'   => __($this->translationKeys . 'datatable.name'),
                'name'          => 'name',
            ],
            [
                'data'          => 'code',
                'column_name'   => __($this->translationKeys . 'datatable.code'),
                'name'          => 'code',
            ],
            [
                'data'          => 'weather',
                'column_name'   => __($this->translationKeys . 'datatable.weather'),
                'name'          => 'weather',
            ],
            [
                'data'          => 'action',
                'column_name'   => __($this->translationKeys . 'datatable.action'),
                'name'          => 'action',
                'sortable'      => false,
                'searchable'    => false,
                'width'         => '5%',
            ],
        ];
    }

    public function getDataForDatatable(): Builder
    {
        return $this->model;
    }


    public function getDatatableElements(): JsonResponse
    {
        $cities = $this->getDataForDatatable();
        return DataTables::of($cities)
            ->addColumn('weather', function ($city) {
                if (!empty($city->weather)) {
                    $weather = json_decode($city->weather);
                    $temperature = round($weather->main->temp - self::T0, 0);
                    $image = '<img src="http://openweathermap.org/img/wn/' . $weather->weather[0]->icon . '@2x.png"/>';
                    return $temperature . ' °C' . $image;
                }
                return '';
            })
            ->addColumn('action', $this->templatePath . 'datatables.action')
            ->rawColumns(['action', 'weather'])
            ->make(true);
    }

    public function storeRecord(array $request): bool
    {
        $city = $this->model->create($request);
        WeatherCity::dispatch($city);
        return true;
    }

    public function updateRecord(array $request, City $city): bool
    {
        return $city->update($request);
    }
}
