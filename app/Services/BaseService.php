<?php

declare(strict_types=1);

namespace App\Services;

class BaseService
{
    /**
     * Функция возвращает нужный формат колонок в json для datatable
     *
     * @return false|string
     */
    public function getJsonTableColumns()
    {
        return json_encode($this->getTableColumns());
    }

    /**
     * Формирует данные для шаблона "Список элементов"
     *
     * @return array
     */
    public function outputData(): array
    {
        return [];
    }
}
