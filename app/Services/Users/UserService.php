<?php

declare(strict_types=1);

namespace App\Services\Users;

use App\Models\User;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class UserService extends BaseService
{
    private $model;
    public $nameController = 'users';
    public $templatePath = 'users.';
    public $templateForm = 'users';
    public $translationKeys = 'users.';

    public function __construct()
    {
        $this->model = User::query();
    }

    public function getElementData(User $user = null)
    {
        return compact('user');
    }

    public function getTableColumns(): array
    {
        return [
            [
                'data'          => 'id',
                'column_name'   => __($this->translationKeys . 'datatable.id'),
                'name'          => 'id',
                'width'         => '10%'
            ],
            [
                'data'          => 'name',
                'column_name'   => __($this->translationKeys . 'datatable.name'),
                'name'          => 'name',
            ],
            [
                'data'          => 'email',
                'column_name'   => __($this->translationKeys . 'datatable.email'),
                'name'          => 'email',
            ],
            [
                'data'          => 'action',
                'column_name'   => __($this->translationKeys . 'datatable.action'),
                'name'          => 'action',
                'sortable'      => false,
                'searchable'    => false,
                'width'         => '5%',
            ],
        ];
    }

    public function getDataForDatatable(): Builder
    {
        return $this->model;
    }


    public function getDatatableElements(): JsonResponse
    {
        $users = $this->getDataForDatatable();
        return DataTables::of($users)
            ->addColumn('action', $this->templatePath . 'datatables.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function storeRecord(array $request): bool
    {
        $this->model->create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ]);
        return true;
    }

    public function updateRecord(array $request, User $user): bool
    {
        return $user->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
    }

    public function createToken($user): User
    {
        $user->forceFill([
            'token' => Str::random(80),
        ])->save();

        return $user;
    }
}
