<?php

declare(strict_types=1);

namespace App\Services\Weather;

use App\Models\Settings\Setting;
use GuzzleHttp\Client;
use Psr\Http\Message\StreamInterface;

class Weather
{
    protected $url = 'api.openweathermap.org/data/2.5/weather';

    protected $client;

    public function get(string $city): StreamInterface
    {
        return $this->query(['q' => $city]);
    }

    public function find($id): StreamInterface
    {
        return $this->query(['id' => $id]);
    }

    public function query(array $search): StreamInterface
    {
        return $this->client()->get("{$this->url}?{$this->queryParameters($search)}")->getBody();
    }

    protected function queryParameters(array $search): string
    {
        return http_build_query(array_merge($this->getOptions(), $search));
    }

    public function getOptions(): array
    {
        $settings = Setting::first();
        return [
            'APPID' =>  $settings->weather_key ?? env('WEATHER_KEY')
        ];
    }

    protected function client(): Client
    {
        return $this->client ?? new Client();
    }

    public function using(Client $client): self
    {
        $this->client = $client;

        return $this;
    }
}
