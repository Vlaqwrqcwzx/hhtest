<a><h1>Weather </h1></a>
## Требования
- [PHP >= 7.3](http://php.net/)
- [MySQL >= 5.7](https://www.mysql.com/)

## Настройки файла .env
<p>Служба db использует готовый образ MySQL 5.7 из Docker Hub. Поскольку Docker Compose 
автоматически загружает файлы переменных .env, находящиеся в той же директории, 
что и файл docker-compose.yml, мы можем получить параметры базы данных из файла 
Laravel .env,</p>
1. Создайте файл .env и заполните 
- <code>DB_DATABASE=travellist</code>
- <code>DB_USERNAME=travellist_user</code>
- <code>DB_PASSWORD=password</code>
- <code>APP_URL=http://localhost</code>

## Разворачивание проекта
1. Клонировать репозиторий <code>git clone </code>
1. Перейти в папку с проектом <code>cd project_name</code>
1. Выполните сборку образа app <code>docker-compose build app</code>
1. Запустить среду в фоновом режиме <code>docker-compose up -d</code>
1. Установить зависимости приложения <code>docker-compose exec app composer install</code>
1. Cгенерировать уникальный ключ приложения <code>docker-compose exec app php artisan key:generate</code>
1. Запустить миграции <code>docker-compose exec app php artisan migrate</code>
1. Запустить сидеры <code>docker-compose exec app php artisan db:seed</code>
1. Открыть <code>localhost:8000</code>

## Команды
1. Обновить данные о погоде городов из системы <code>docker-compose exec app php artisan weather:parse</code>

## API
1. POST /auth  JSON {login, password} -> JSON {token}
1. GET /weather headers {token} -> JSON {city, current_temp}

## Тестирование
<p>Class для тестирования методов API <code>ApiTest.php</code></p>
<p>Ауентификация <code>testAuthApi</code></p>
<p>Получение погоды <code>testWeatherApi</code></p>

## Вход в систему /login
![](public/images/1.png)

## Тестовый данные для входа 
- email: test@mail.ru
- пароль: test

## Регистрация /register
![](public/images/2.png)

## Добавление нового пользователя через систему
![](public/images/6.png)
- нажмите зелёную кнопку "Добавить пользователя"

![](public/images/7.png)
- заполните все поля и нажмите зеленую кнопку "Сохранить и добавить"

![](public/images/8.png)

## Добавление нового города
![](public/images/3.png)
- система не создаст города, которого не существует
- перейдите в список городов
- нажмите зелёную кнопку "Добавить город"

![](public/images/4.png)
- "Название города" обязательно для заполнения
- "Код города" необязательное поле, будет использоваться в будущеем , для парсера погоды не только по названию города , но и по коду города

![](public/images/5.png)
- при добавлении города, запускается задача для определения погоды в городе

## Настройки системы
![](public/images/9.png)
- Openweathermap API_KE ключ для доступа к апи сервиса погоды, можно указать в файле .env
- Время обновления погоды городов в системе
