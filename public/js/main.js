$(document).ready(function () {
    $('[timepicker]').timepicker({
        timeFormat: 'HH:mm',
        interval: 60,
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
});

var Main = {
    // Базовые настройки для Datatables
    commonConfigDt: {
        pageLength: 50,
        processing: true,
        serverSide: true,
        order: [ ],
        language: {
            processing: 'Загружается...',
            search: "Поиск: ",
            lengthMenu: "Показать _MENU_  записей",
            info: "Записи с _START_ по _END_ из _TOTAL_ ",
            infoEmpty: "",
            infoFiltered: "",
            infoPostFix: "",
            loadingRecords: "",
            zeroRecords: "Ничего не найдено",
            emptyTable: "Записей пока нет",
            paginate: {
                first: "",
                previous: "Назад",
                next: "Вперед",
                last: ""
            },
            aria: {
                sortAscending: "",
                sortDescending: ""
            }
        },
        scrollX: true,
        stateSave: true,
    },

    confDrp: {
        "format": "DD.MM.YYYY",
        "formatDateTime": "DD.MM.YYYY HH:mm",
        "separator": " - ",
        "applyLabel": "Применить",
        "cancelLabel": "Отменить",
        "fromLabel": "От",
        "toLabel": "До",
        "customRangeLabel": "Custom",
        "weekLabel": "W",
        "daysOfWeek": [
            "Вс",
            "Пн",
            "Вт",
            "Ср",
            "Чт",
            "Пт",
            "Сб"
        ],
        "monthNames": [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        ],
        "firstDay": 1
    },

    autocompleteLanguageConfig: {
        errorLoading: function () {
            return 'Результат не может быть загружен.';
        },
        inputTooLong: function (args) {
            var overChars = args.input.length - args.maximum;
            var message = 'Пожалуйста, удалите ' + overChars + ' символ';
            if (overChars >= 2 && overChars <= 4) {
                message += 'а';
            } else if (overChars >= 5) {
                message += 'ов';
            }
            return message;
        },
        inputTooShort: function (args) {
            var remainingChars = args.minimum - args.input.length;

            var message = 'Пожалуйста, введите ' + remainingChars + ' или более символов';

            return message;
        },
        loadingMore: function () {
            return 'Загружаем ещё ресурсы…';
        },
        maximumSelected: function (args) {
            var message = 'Вы можете выбрать ' + args.maximum + ' элемент';

            if (args.maximum  >= 2 && args.maximum <= 4) {
                message += 'а';
            } else if (args.maximum >= 5) {
                message += 'ов';
            }

            return message;
        },
        noResults: function () {
            return 'Ничего не найдено';
        },
        searching: function () {
            return 'Поиск…';
        }
    },

    tagSelect2: function (params) {
        var term = $.trim(params.term);
        if (term === '') {
            return null;
        }
        return {
            id: term,
            text: term,
            newTag: true
        }
    },

    initSelect2: function(dataName, route, createTag = function (params) {}, dropdownParent){
        $('[data-name="' + dataName + '"]').select2({
            'ajax': {
                url: route,
                dataType: 'json',
                type: 'POST',
                cache: true,
                data: function (params) {
                    return {
                        'string': params.term,
                        '_token': $('meta[name="csrf-token"]').attr('content'),
                    };
                },
                processResults: function (result) {
                    return {
                        results: result,
                    };
                },
            },
            'allowClear': true,
            'createTag': createTag,
            'language': Main.autocompleteLanguageConfig,
            'minimumInputLength': '2',
            'placeholder': "Ничего не выбрано",
            'width': '100%',
            'dropdownParent': dropdownParent
        });
    },


    previewImage: function(element) {
        let attribute = $(element).data('id');
        if ($(element).prop('multiple')) {
            $(element).prev('span').html('(выбрано ' + element.files.length + ' файлов)');
        } else {
            let file = element.files[0];
            let id = $('#' + attribute);
            if (file) {
                id.attr('href', '');
                id.find('img').attr('src', '');
                var reader = new FileReader();
                reader.onload = function (e) {
                    id.attr('href', e.target.result);
                    id.find('img').attr('src', e.target.result);
                };
                reader.readAsDataURL(file);
            }
        }

    },

    // На некоторых страницах приходит форма с редактором через AJAX. Инициализация в методе
    summernote: function (element = '#summernote') {
        $(element).summernote({
            lang: 'ru-RU',
            height: '100',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ],
            callbacks: {
                onFocus: function(e) {
                    if (typeof routes !== "undefined") {
                        Chat.readMessages(routes.read_msg);
                    }
                },
            }
        });
    },

    // Поиск в таблице
    searchDataTable: function(table, input){
        if(table && input){
            table.search($(input).val()).draw();
        }
        return true;
    },

    updateDataTable: function(e, table){
        if(!table){
            table = window.LaravelDataTables["dtListElements"];
        }
        table.draw();
        e.preventDefault();
    },

    // Удаление элемента
    deleteDataTable: function (route) {
        if (confirm('Собираетесь удалить элемент из системы?')) {

            let data = new FormData();
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));
            data.append('_method', 'DELETE');

            var success = function (data) {
                if (data['action'] && data['action'] === 'reload_table') {
                    dtListelements.ajax.reload(null, false);
                    toastr.error('Удалено');
                }
            };

            var error = function(data) {
                if (data.status === 403) {
                    toastr.error('Недостаточно прав')
                }
            }

            this.ajaxRequest('POST', route, data, success, error);
        }
    },



    // Подстановка в строки таблицы ссылок на просмотр элемента. Требуется скрытая ссылка с классом data-table-show и роутом
    showElementDataTable: function(table) {
        $('#'+ table +' tr').each(function(i,e) {
            $(e).children('td').click(function() {
                if($(this).find("a,input,select,button,img").length == 0) {
                    let atrr = $(e).find('.data-table-show').closest('a');
                    location.href = atrr.attr('href');
                }
            });
        });
    },

    showModalElementDataTable: function(table) {
        $('#'+ table +' tr').each(function(i,e) {
            $(e).children('td').click(function() {
                if($(this).find("a,input,select,button,img").length == 0) {
                    let attr = $(e).find('.data-table-show').closest('a');
                    attr.trigger('click');
                }
            });
        });
    },

    editElementDataTable: function(table) {
        $('#'+ table +' tr').each(function(i,e) {
            $(e).children('td').click(function() {
                if($(this).find("a,input,select,button,img").length == 0) {
                    let atrr = $(e).find('.data-table-edit').closest('a');
                    location.href = atrr.attr('href');
                }
            });
        });
    },


    // Отправка ajax запроса на сервер
    ajaxRequest: function(typeRequest, urlRequest, dataRequest, successFunction, errorFunction, returnFunction){
        if(typeRequest && urlRequest && typeof dataRequest == 'object' && successFunction){
            let request = $.ajax({
                type   		: typeRequest,
                url    		: urlRequest,
                cache  		: false,
                data 	  	: dataRequest,
                processData	: false,
                contentType	: false,
                success 	: successFunction,
                error		: errorFunction
            });
            if(returnFunction === true)
                return request;
            else
                return true;
        }
        return false;
    },

    modalWithConfirm: null,

    modalConfirm: function(text, action){
        let thisGeneral = this;

        if(text && action){

            text = '<p>' + text + '</p>';

            if(thisGeneral.modalWithConfirm){
                thisGeneral.modalWithConfirm.changeBody(text);
            } else {
                thisGeneral.modalWithConfirm = new ModalApp.ModalProcess({
                    id: 'modal_with_confirm',
                    title: 'Подтвердите действие:',
                    body: text,
                    style: 'width: 30%;',
                    g_style: 'z-index: 1100',
                    footer: '<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>\
							<button type="button" onclick="' + action + '" class="btn btn-success">Хорошо</button>'
                });
            }

            thisGeneral.modalWithConfirm.init();
            thisGeneral.modalWithConfirm.showModal();

            return true;
        }
        return false;
    },

    destroyElement: null,

    deleteMethodLE: function(urlDestroy, elementName, table, usFunc){
        let thisGeneral = this;

        if(urlDestroy){
            thisGeneral.destroyElement = function () {
                if(!table){
                    table = window.LaravelDataTables["dtListElements"];
                }

                let data = new FormData();
                data.append('_token', config.token);
                data.append('_method', 'DELETE');

                let successFunction = function (data) {
                    if(data['action'] && data['action'] == 'reload_table'){
                        table.ajax.reload( null, false );
                    }

                    if(data['error']) {
                        alert(data['error']);
                        throw new Error(data['error']);
                    }


                    if(usFunc){
                        usFunc();
                    }
                };

                var error = function(data) {
                    if (data.status === 403) {
                        toastr.error('Недостаточно прав')
                    }
                };


                thisGeneral.ajaxRequest('POST', urlDestroy, data, successFunction, error);

                if(elementName != undefined && elementName != null){
                    thisGeneral.modalWithConfirm.hideModal();
                }
            };

            if(elementName === undefined || elementName === null){
                thisGeneral.destroyElement();
            } else {
                thisGeneral.modalConfirm('Вы действительно хотите удалить запись с наименованием "' + elementName + '"?', 'Main.destroyElement();');
            }

        }

        return true;
    },

    // Снятие всех фильтров
    dropFilter: function () {
        $('[data-name="filter"]').val('');
        $('input[type="checkbox"][data-name="filter"]').prop("checked", false);
        $('[data-name="filter"]').trigger('change');
    },

    // Открытие модального окна формы создания записи
    createRecord: function (route) {
        var data = {
            _method: 'GET',
        };
        var success = function (data) {
            if (data.action == 'success') {
                let html = $.parseHTML(data.html);
                $( "#create" ).remove();
                $('#form').find(".modal-header").after(html);
                $('#form').find('.modal-title-form').text(data.title);
                $('#form').find('.modal-button-form').text(data.button);
                $.getScript( "/js/main.js" )
                $('#form').modal('show');
            }
        };

        var error = function(data) {
            if (data.status === 403) {
                toastr.error('Недостаточно прав')
            }
        }

        this.ajaxRequest('GET', route, data, success, error);
    },

    // Открытие модального окна формы редактирования записи
    editRecord: function (route) {
        var data = {
            _method: 'GET',
        };

        var success = function (data) {
            if (data.action == 'success') {
                let html = $.parseHTML(data.html);
                $( "#create" ).remove();
                $('#form').find(".modal-header").after(html);
                $('#form').find('.modal-title-form').text(data.title);
                $('#form').find('.modal-button-form').text(data.button);
                $.getScript( "/js/main.js" )
                $('#form').modal('show');
            }
        };

        var error = function(data) {
            if (data.status === 403) {
                toastr.error('Недостаточно прав')
            }
        }

        this.ajaxRequest('GET', route, data, success, error);
    },

    // Обновление записи в базе данных
    updateRecord: function (route) {

        var data = new FormData($('#create')[0]);
        data.append('_method', 'PATCH');
        var success = function (data) {
            if (data.action == 'reload_table') {
                toastr.success(data['success']);
                dtListelements.ajax.reload(null, false);
                $('#form').modal('hide');
                $('#create').remove();
            }
        };

        var error = function (data) {
            $.each(data.responseJSON.errors, function (index, error) {
                toastr.error(error[0]);
            });

            if (data.status && data.status === 403) {
                toastr.error('Недостаточно прав')
            }
        };

        this.ajaxRequest('POST', route, data, success, error);
    },

    // Создание записи в базе данных
    storeRecord: function (route) {
        var data = new FormData($('#create')[0]);
        var success = function (data) {
            if (data.action == 'reload_table') {
                toastr.success(data['success']);
                dtListelements.ajax.reload(null, false);
                $('#form').modal('hide');
                $('#create').remove();
            }
        };

        var error = function (data) {
            $.each(data.responseJSON.errors, function (index, error) {
                toastr.error(error[0]);
            });

            if (data.status && data.status === 403) {
                toastr.error('Недостаточно прав')
            }
        };

        this.ajaxRequest('POST', route, data, success, error);
    },

    // Скрытие модального окна с удалением формы внутри него
    dissmissModal: function (id) {
        $(id).modal('hide');
        $(id).find('form').remove();
    },

    disableToggleElement: function(id) {
        var element = document.querySelector('#' + id);
        element.toggleAttribute("disabled");
    },

    readNotification: function(route, id) {
        $('[data-notification-id="'+ id + '"').remove();
        var count = $('[data-name="notifications"] span').text();
        var result = count - 1;
        $('[data-name="notifications"] span').text(result);
        if ($('[data-name="notifications"] span').text() == 0) {
            $('[data-name="notifications"] span').remove();
            $('[data-id="notifications"]').remove();

        }
        var data = new FormData();
        data.append('_method', 'POST');
        data.append('_token', config.token);
        var success = function (data) {

        };

        this.ajaxRequest('POST', route, data, success);
    },
    eventRemind: function(element, date, key, value) {
        if (date != "") {
            let list = date.split(' ');
            let timeList = list[1].split(':');
            let dateList = list[0].split('.');
            let newDate = new Date(dateList[2], dateList[1], dateList[0], timeList[0], timeList[1]);
            switch (key) {
                case 'minute':
                    newDate.setMinutes(newDate.getMinutes() - value);
                    break;
                case 'hour':
                    newDate.setHours(newDate.getHours() - value);
                    break;
                case 'day':
                    newDate.setDate(newDate.getDate() - value);
                    break;
            }
            let month = newDate.getMonth();
            if (month < 10)
                month = '0' + month
            let stringDate = newDate.getDate() + '.' + month + '.' + newDate.getFullYear() + ' ' + newDate.getHours() + ':' + newDate.getMinutes();
            element.val(stringDate);
            return true;
        } else {
            alert('Выберите дату события!');
        }
    }

};
