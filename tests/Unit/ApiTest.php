<?php

declare(strict_types=1);

namespace Tests\Unit;

use GuzzleHttp\Client;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

class ApiTest extends TestCase
{
    public function testAuthApi(): void
    {
        $client = new Client();
        $response = $client->post(
            'http://localhost:8000/api/v1/auth',
            [
                'json' => [
                    'email' => 'fedotow.vla2017@yandex.ru',
                    'password' => '111111'
                ]
            ],
            ['Content-Type' => 'application/json'],
        );

        $responseJSON = json_decode($response->getBody(), true);
        var_dump($responseJSON);

        $this->assertEquals(Str::length($responseJSON['data']['token']), 80);
    }

    public function testWeatherApi(): void
    {
        $headers = [
            'token' => 'rj9JvmayJuJWBZerWSr1u8dB4ooo9vjecgUPCNWlyLpzqHG7pNlnHf6pjneXkFLigTdmDoXI2OsogcaU',
        ];
        $client = new Client([
            'headers' => $headers
        ]);
        $response = $client->get('http://localhost:8000/api/v1/weather');
        $responseJSON = json_decode($response->getBody(), true);
        var_dump($responseJSON);

        $this->assertEquals(true);
    }
}
