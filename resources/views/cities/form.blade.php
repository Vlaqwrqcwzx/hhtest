<form id="create" method="POST" enctype="multipart/form-data" role="form">
    @csrf
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Название<sup class="text-danger">*</sup></label>
                    <input name="name" type="text" class="form-control" placeholder="Москва" required
                           value="{{ $city->name ?? old('title') }}"
                    >
                </div>
                <div class="form-group">
                    <label>Код</label>
                    <input name="code" type="text" class="form-control" placeholder="код города" required
                           value="{{ $city->code ?? old('code') }}"
                    >
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0)" class="btn btn-success modal-button-form"
           @if(isset($city)) onclick="Main.updateRecord('{{ route('cities.update', $city->id) }}')"
           @else onclick="Main.storeRecord('{{ route('cities.store') }}')" @endif
        >Сохранить и добавить</a>
        <button type="button" class="btn btn-danger" onclick="Main.dissmissModal('#form')">Отмена</button>
    </div>
</form>

