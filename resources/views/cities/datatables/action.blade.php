<div class="d-flex m-button">
    <button type="button" class="btn btn-success btn-sm mr-1"
            onclick="Main.editRecord('{{ route('cities.edit', $id) }}')">
        <i class="fas fa-edit"></i>
    </button>

    <a onclick="Main.deleteDataTable('{{ route('cities.destroy', $id) }}')" href="javascript:void(0);"
       class="btn btn-sm btn-danger">
        <i class="fas fa-trash-alt"></i>
    </a>
</div>
