@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ action(App\Http\Controllers\Settings\SettingController::class) }}" method="POST" enctype="multipart/form-data" role="form">
                        <input name="_method" type="hidden" value="PATCH">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="weather_key">Openweathermap API_KEY<sup class="text-danger">*</sup></label>
                                    <input type="text" name="weather_key" value="{{ $settings->weather_key ?? @old('weather_key') }}"
                                           class="form-control" id="weather_key" placeholder="6397bf0d4ac8a49ef3542719b101ba14" required>
                                </div>
                                <div class="form-group">
                                    <label>Время обновления погоды<sup class="text-danger">*</sup></label>
                                    <input name="parse_at" type="text" class="form-control" required
                                           data-position="right top"
                                           timepicker
                                           data-date-format="hh:ii"
                                           data-timepicker="true"
                                           placeholder="16:24"
                                           value="{{ $settings->parse_at ?? @old('parse_at') }}"
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-success" type="submit">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
