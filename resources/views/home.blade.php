@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center">Привет, {{ auth()->user()->name }} !</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row justify-content-center">
                            <p>Добро пожаловать в моё тестовое приложение :)</p>
                        </div>

                        <div class="row justify-content-center mt-3">
                            <img src="{{ asset('images/tenor.gif') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
