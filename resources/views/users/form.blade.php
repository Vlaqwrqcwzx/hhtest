<form id="create" method="POST" enctype="multipart/form-data" role="form">
    @csrf
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="name">Имя <sub class="text-danger">*</sub></label>
                    <input type="text" name="name" value="{{ $user->name ?? @old('name') }}" class="form-control" id="name" placeholder="Иван" required>
                </div>
                <div class="form-group">
                    <label for="email">Email <sub class="text-danger">*</sub></label>
                    <input type="email" name="email" value="{{ $user->email ?? @old('email') }}" class="form-control" id="email" placeholder="mail@mail.ru" required>
                </div>
                <div class="form-group">
                    <label for="password">Пароль <sub class="text-danger">*</sub></label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                           placeholder="Password" required>
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Повторите пароль <sub class="text-danger">*</sub></label>
                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation"
                           placeholder="Confirm Password" required>
                </div>

            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0)" class="btn btn-success modal-button-form"
           @if(isset($user)) onclick="Main.updateRecord('{{ route('users.update', $user->id) }}')"
           @else onclick="Main.storeRecord('{{ route('users.store') }}')" @endif
        >Сохранить и добавить</a>
        <button type="button" class="btn btn-danger" onclick="Main.dissmissModal('#form')">Отмена</button>
    </div>
</form>

